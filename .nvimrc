" vim:set et sw=4 ts=4 tw=78:
set encoding=utf-8
scriptencoding utf-8


" Don't cd into the directory of the current file
let g:autocd = 0
