---
#
# Notes: $CI_COMMIT_REF_NAME => master or branch-xyz
#

#########################################################################
#  credentials are to be added securely as variables in a GitLab panel  #
#########################################################################
variables:
  PACKAGE: csnake
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PRE_COMMIT_HOME: "$CI_PROJECT_DIR/.cache/pre-commit"

###############################################################################
#                                Global setup                                 #
###############################################################################
image: python:3.9

cache:
  - key:
      prefix: $CI_JOB_IMAGE
      files:
        - poetry.lock
        - .gitlab-ci.yml
    paths:
      - .cache/pip
      - .venv

  - key:
      prefix: $CI_JOB_IMAGE
      files:
        - .pre-commit-config.yaml
    paths:
      - .cache/pre-commit

before_script:
  - pip install poetry
  - poetry config virtualenvs.in-project true
  - poetry install --no-root --no-interaction
  - export VERSION=$(git describe --tags --abbrev=0)
  - poetry version "$VERSION"
  - echo "__version__ = \"$VERSION\"" > $PACKAGE/_version.py

stages:
  - lint
  - build
  - test
  - test_deploy
  - deploy
  - after_deployment
  - cleanup

###############################################################################
#                                 lint stage                                  #
###############################################################################

# don't even try doing anything if the commit wasn't properly linted pre-commit

#########################################
#  pre-commit hooks re-run server-side  #
#########################################

lint:
  stage: lint
  script:
    - poetry run pre-commit run --all-files --color=always

readme_lint:
  stage: lint
  script:
    - poetry run python -m readme_renderer README.rst -o /dev/null

###############################################################################
#                                 test stage                                  #
###############################################################################

##########################
#  run pytest + codecov  #
##########################
.pytest_codecov: &pytest_common
  stage: test
  script:
    - poetry run pip install dist/$PACKAGE-$VERSION-py3-none-any.whl
      --exists-action w
      --force-reinstall
    - poetry run pytest
      --color=yes
      --full-trace
      --showlocals
      --verbose

##############################################
#  pytest tests on multiple python versions  #
##############################################

pytest:
  artifacts:
    paths:
      - htmlcov/
      - pytest_report.html
  stage: test
  script:
    - poetry run pip install dist/$PACKAGE-$VERSION-py3-none-any.whl
      --exists-action w
      --force-reinstall
    - poetry run pytest
      --cov=${PACKAGE}
      --cov-report html
      --html=pytest_report.html
      --self-contained-html
      --color=yes
      --full-trace
      --showlocals
      --verbose

pytest38:
  image: python:3.8
  <<: *pytest_common

pytest37:
  image: python:3.7
  <<: *pytest_common

##############
##  doctest  #
##############
doctest:
  stage: test
  script:
    - export PYTHONPATH=$PWD
    - cd docs
    - poetry run make doctest

###############################################################################
#                                 build stage                                 #
###############################################################################

#################
#  build wheel  #
#################
wheel:
  stage: build
  environment: deploy
  script:
    - poetry build --no-interaction
  artifacts:
    paths:
      - dist


##############################################################
#  Sphinx documentation (generated into 'docs/_build/html')  #
##############################################################
doc:
  stage: build
  script:
    - export PYTHONPATH=$PWD
    - cd docs
    - poetry run make html
    - cd ..
    - ls docs/_build/html
  artifacts:
    paths:
      - docs/_build/html

###############################################################################
#                                deploy stage                                 #
###############################################################################

#########################
#  upload to test PyPI  #
#########################
deploy_test_pypi:
  stage: test_deploy
  dependencies:
    - wheel
  environment:
    name: pypi
    url: https://pypi.org/project/$PACKGE/
  script:
    - poetry config repositories.test https://test.pypi.org/legacy/
    - >
      poetry publish
      --repository "test"
      --username "__token__"
      --password "$TOKEN_TEST_PYPI"
  only:
    - tags


##########################################
#  Sphinx documentation to GitLab pages  #
##########################################
# Create the pages into http://andrejr.gitlab.io/$PACKAGE
pages:
  stage: deploy
  script:
    - ls
    - mv docs/_build/html public
    - mv htmlcov public/coverage
    - mv pytest_report.html public
    - ls public
  dependencies:
    - doc
    - pytest
  artifacts:
    paths:
      - public
  only:
    - tags

####################
#  upload to PyPI  #
####################
deploy_pypi:
  stage: deploy
  dependencies:
    - wheel
  environment:
    name: pypi
    url: https://pypi.org/project/$PACKAGE/
  script:
    - >
      poetry publish
      --username "__token__"
      --password "$TOKEN_PYPI"
  only:
    - tags

###############################################################################
#                           after_deployment stage                            #
###############################################################################

###################
#  just coverage  #
###################
coverage:
  stage: after_deployment
  script:
    - poetry run python -m pytest --cov=${PACKAGE}
  coverage: '/^TOTAL.+?(\d+\%)$/'
